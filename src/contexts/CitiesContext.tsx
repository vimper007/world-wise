import { ReactNode, createContext, useContext, useEffect, useReducer, useState } from "react";
import { CityType, IsLoadingType, NewCityType } from "../types";
const BASE_URL = 'http://localhost:8000'

const defaultCity: CityType = {
  cityName: '',
  country: '',
  emoji: '',
  date: new Date(),
  notes: '',
  position: {
    lat: 0,
    lng: 0,
  },
  id: '',
};

// const defaultNewCity: NewCityType = {
//   cityName: '',
//   country: '',
//   date: new Date(),
//   notes: '',
//   position: {
//     lat: '0',
//     lng: '0',
//   },

// }

const defaultValue: ContextType = {
  cities: [defaultCity],
  isLoading: false,
  currentCity: defaultCity,
  getCity: async () => Promise.resolve(),
  createCity: async () => Promise.resolve(),
  deleteCity: async () => Promise.resolve(),
};
const CitiesContext = createContext<ContextType>(defaultValue);

type CitiesContextProviderProps = {
  children: ReactNode,
}

type ContextType = {
  cities: CityType[];
  isLoading: boolean;
  currentCity: CityType;
  getCity(id: number): Promise<void>;
  createCity(city: NewCityType): Promise<void>;
  deleteCity(id: string): Promise<void>;
}

type InitialStateType = {
  cities: CityType[];
  isLoading: boolean;
  currentCity: CityType;
  currentCityID:string;
  error: string,
}

type ActionType = 
  | { type: 'cities/loaded'; payload: CityType[] }
  | { type: 'cities/created' }
  | { type: 'loading'; }
  | { type: 'rejected'; payload: string }
  | { type: 'cities/deleted'; payload: string }
  | { type: 'city/loaded'; payload: CityType }

function reducer(state: InitialStateType, action: ActionType) {
  switch (action.type) {
    case ('loading'):
      return { ...state, isLoading: true }
    case ('cities/loaded'):
      return { ...state, cities: action.payload }
    case ('cities/created'):
      return { ...state, isLoading: true }
    case ('cities/deleted'):
      return { ...state, currentCityID: action.payload }
    case ('rejected'):
      return { ...state, isLoading: false, error: action.payload }
    case ('city/loaded'):
      return { ...state, isLoading: false, currentCity: action.payload }
    default:
      return state
  }
}

const initialState: InitialStateType = {
  cities: [defaultCity],
  isLoading: false,
  currentCity: defaultCity,
  error: '',
  currentCityID:'',
}


function CitiesContextProvider({ children }: CitiesContextProviderProps) {
  const [, dispatch] = useReducer(reducer, initialState)
  const [cities, setCities] = useState<CityType[]>([])
  const [isLoading, setIsLoading] = useState<IsLoadingType>(false)
  const [currentCity, ] = useState<CityType>(defaultCity)

  useEffect(() => {
    async function fetchCities() {
      dispatch({ type: 'loading' })
      try {
        const res = await fetch(`${BASE_URL}/cities`);
        const data = await res.json();
        setCities(data)
      }
      catch {
        alert('Error....');
        dispatch({ type: 'rejected', payload: 'There was an error loading cities...' })
      }
      finally {
        setIsLoading(false)
      }
    }
    fetchCities();
  }, [])

  async function getCity(id: number) {
    dispatch({ type: 'loading' })
    try {
      const res = await fetch(`${BASE_URL}/cities/${id}`);
      const data = await res.json();
      dispatch({type:'city/loaded',payload:data})
    } catch (error) {
      dispatch({ type: 'rejected', payload: 'There was an error loading city...' })
    } 
  }

  async function createCity(newCity: NewCityType) {
    dispatch({ type: 'loading' })
    try {
      const res = await fetch(`${BASE_URL}/cities`, {
        method: 'POST',
        body: JSON.stringify(newCity),
        headers: {
          'Content-Type': 'application/JSON'
        }
      })
      const data = await res.json();
      console.log(data)
      setCities(prev => [...prev, data])
    } catch (error) {
      alert(error);
      console.log(error);
    } 

  }

  async function deleteCity(id: string) {
    dispatch({type:'loading'})
    try {
      await fetch(`${BASE_URL}/cities/${id}`, {
        method: "DELETE"
      });
      dispatch({type:'cities/deleted',payload:id})
      setCities(cities => cities.filter(city => city.id !== id));
    } catch (error) {
      console.log(error)
    } 
  }

  return <CitiesContext.Provider value={{
    cities,
    isLoading,
    currentCity,
    getCity,
    createCity,
    deleteCity
  }}>
    {children}
  </CitiesContext.Provider>
}



function useCities() {
  const context = useContext<ContextType>(CitiesContext);
  if (context === undefined) throw new Error('useCities cannot be used outside the CitiesContext provider');
  return context;
}

export { CitiesContextProvider, useCities }