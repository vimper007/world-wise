import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom"

import Product from "./pages/Product"
import Pricing from "./pages/Pricing"
import Homepage from "./pages/Homepage"
import PageNotFound from "./pages/PageNotFound"
import AppLayout from "./pages/AppLayout"
import Login from "./pages/Login"
import CityList from "./components/CityList"
import CountryList from "./components/CountryList"
import City from "./components/City"
import Form from "./components/Form"
import { CitiesContextProvider } from "./contexts/CitiesContext"

// const BASE_URL = 'http://localhost:8000'

function App() {
  // const [cities, setCities] = useState<CityType[]>([])
  // const [IsLoading, setIsLoading] = useState<IsLoadingType>(false)

  // useEffect(() => {
  //   setIsLoading(true);
  //   async function fetchCities() {
  //     try{
  //       const res = await fetch(`${BASE_URL}/cities`);
  //       const data = await res.json();
  //       setCities(data)
  //     }
  //     catch{
  //       alert('Error....')
  //     }
  //     finally{
  //       setIsLoading(false)
  //     }
  //   }
  //   fetchCities();
  // }, [])


  return (
    <CitiesContextProvider>
      <BrowserRouter>
        <Routes>
          <Route index element={<Homepage />} />
          <Route path="pricing" element={<Pricing />} />
          <Route path="product" element={<Product />} />
          <Route path="login" element={<Login />} />
          <Route path="app" element={<AppLayout />} >
            <Route index element={<Navigate to='cities' replace/>} />
            <Route path="cities" element={<CityList />} />
            <Route path="cities/:id" element={<City/>}/>
            <Route path="countries" element={<CountryList ></CountryList>} />
            <Route path="form" element={<Form/>} />
          </Route>
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
      </CitiesContextProvider>
  )
}

export default App