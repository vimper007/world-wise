export type CityListProps = {
  cities: CityType[]
  isLoading: IsLoadingType
}

export type CityType = {
  cityName: string,
  country: string,
  emoji: string,
  date: Date,
  notes: string,
  position: {
    lat: number,
    lng: number
  },
  id: string
}

export type IsLoadingType = boolean

export type ChangeCenterProps = {
  position: PositionType
}
export type PositionType = {
  lat: number;
  lng: number;
} | [number, number]

export type NewCityType = {
  cityName: string;
  country: string;
  date: Date | null;
  notes: string;
  position: {
    lat: string | null;
    lng: string | null;
  };
}