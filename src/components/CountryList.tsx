import { useCities } from '../contexts/CitiesContext'
import { CityType } from '../types'
import CountryItem from './CountryItem'
import styles from './CountryList.module.css'
import Message from './Message'
import Spinner from './Spinner'


function CountryList() {
    const { cities, isLoading } = useCities()
    if (isLoading) return <Spinner />

    if (!cities?.length) return <Message message='Add your first city by clicking on the map!'></Message>

    const countries = cities.reduce((acc: CityType[], curr: CityType) => {
        if (!acc.some(item => item.country === curr.country)) {
            return [...acc, curr]
        } else {
            return acc
        }
    }, [])
    return (
        <ul className={styles.countryList}>
            {
                countries?.map(country => <CountryItem country={country} key={country.id}></CountryItem>)
            }
        </ul>
    )
}

export default CountryList