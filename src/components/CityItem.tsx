import { Link } from "react-router-dom";
import { CityType } from "../types"
import styles from './CityItem.module.css'
import { useCities } from "../contexts/CitiesContext";
import { MouseEvent } from "react";

type Props = {
    city: CityType;
}

function formatDate(date: Date) {
    return new Intl.DateTimeFormat("en", {
        day: "numeric",
        month: "long",
        year: "numeric"
    }).format(new Date(date))
}



function CityItem({ city }: Props) {
    const { currentCity,deleteCity } = useCities()
    const { cityName, emoji, date, id, position, } = city;

    function handleDelete(e:MouseEvent<HTMLButtonElement>){
        e.preventDefault();
        deleteCity(id)
    }
    return (
        <li >
            <Link to={`${id}?lat=${position.lat}&lng=${position.lng}`}
                className={`${styles.cityItem} ${id === currentCity.id ? styles['cityItem--active'] : ''}`}>
                <span className={styles.emoji}>{emoji}</span>
                <h3 className={styles.name}>{cityName}</h3>
                <time dateTime={date.toString()}>{formatDate(date)}</time>
                <button className={styles.deleteBtn} onClick={handleDelete}>&times;</button>
            </Link>
        </li>
    )
}

export default CityItem