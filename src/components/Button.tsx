import { MouseEvent, ReactNode } from 'react'
import styles from './Button.module.css'

type ButtonProps = {
  children: ReactNode;
  onClick: (e:MouseEvent<HTMLButtonElement>) => void;
  type: 'back' | 'primary' | 'position';
}

function Button({ children, onClick, type }: ButtonProps) {
  return (
    <button onClick={onClick} className={`${styles.btn} ${styles[type]}`}>
      {children}
    </button>
  )
}

export default Button