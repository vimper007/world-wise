import { useNavigate } from 'react-router-dom'
import styles from './Map.module.css'
import { MapContainer, Marker, Popup, TileLayer, useMap, useMapEvents } from 'react-leaflet';
import { useEffect, useState } from 'react';
import { useCities } from '../contexts/CitiesContext';
import { ChangeCenterProps } from '../types';
import { useGeoLocation } from '../hooks/useGeoLocation';
import Button from './Button';
import { useUrlPosition } from '../hooks/useUrlPosition';

function Map() {
  const { cities } = useCities();
  const navigate = useNavigate();
  const [lat,lng] = useUrlPosition();
  const { getPosition, isLoading: isGeoLocationLoading,
    position: geoLocationPosition
  } = useGeoLocation();
  
  // const [searchParams] = useSearchParams();
  // const lat = searchParams.get('lat');
  // const lng = searchParams.get('lng');
  const [mapPosition, setMapPosition] = useState<{ lat: number, lng: number }|[number,number]>([10,12]);

  useEffect(() => {
    if (lat !== null && lng !== null) {
      setMapPosition([parseFloat(lat),parseFloat(lng)]);
    }
  }, [lat, lng])

  useEffect(() => {
    if (geoLocationPosition) setMapPosition(geoLocationPosition)
  }, [geoLocationPosition])

  return (
    <div className={styles.mapContainer} onClick={() => navigate('form')}>
      {!geoLocationPosition &&
        <Button type='position' onClick={getPosition} >
          {isGeoLocationLoading ? 'Loading...' : 'Use your position'}
        </Button>
      }
      <MapContainer
        center={mapPosition}
        zoom={6} scrollWheelZoom={true}
        className={styles.map}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"
        />
        {cities.map(city => {
          return <Marker position={[city.position.lat, city.position.lng]} key={city.id}>
            <Popup>
              <span>{city.emoji} {city.cityName}</span>
            </Popup>
          </Marker>
        })}
        <ChangeCenter position={mapPosition} />
        <DetectClick />
      </MapContainer>
    </div>
  )

  function ChangeCenter({ position }: ChangeCenterProps) {
    const map = useMap();
    map.setView(position)
    return null
  }

}

function DetectClick() {
  const navigate = useNavigate();
  useMapEvents({
    click: e => {
      if(e.latlng.lat && e.latlng.lng)
      navigate(`form?lat=${e.latlng.lat}&lng=${e.latlng.lng}`)
    }
  })
  return null
}
export default Map