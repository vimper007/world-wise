// "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=0&longitude=0"

import { FormEvent, useEffect, useState } from "react";
import "react-datepicker/dist/react-datepicker.css";

import styles from "./Form.module.css";
import Button from "./Button";
import BackButton from "./BackButton";
import { useUrlPosition } from "../hooks/useUrlPosition";
import Message from "./Message";
import Spinner from "./Spinner";
import DatePicker from "react-datepicker";
import { useCities } from "../contexts/CitiesContext";
import { useNavigate } from "react-router-dom";

const BASE_URL = 'https://api.bigdatacloud.net/data/reverse-geocode-client';

export function convertToEmoji(countryCode: string) {
  const codePoints = countryCode
    .toUpperCase()
    .split("")
    .map((char) => 127397 + char.charCodeAt(0));
  return String.fromCodePoint(...codePoints);
}

function Form() {

  const [cityName, setCityName] = useState("");
  const [country, setCountry] = useState('')
  const [isLoadingGeocoding, setIsLoadingGeocoding] = useState(false)
  const [date, setDate] = useState<Date | null>(new Date());
  const [notes, setNotes] = useState("");
  const [lat, lng] = useUrlPosition();
  const [emoji, setEmoji] = useState('');
  const [geocodingError, setGeocodingError] = useState("");
  const [currentMapLocationData, setCurrentMapLocationData] = useState<{ lat: string, lng: string }>({ lat: '0', lng: '0' });
  const { createCity, isLoading } = useCities();
  const navigate = useNavigate();

  async function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (!cityName || !date) return;
    const newCity = {
      cityName,
      country,
      date,
      emoji,
      notes,
      position: { lat: currentMapLocationData?.lat, lng: currentMapLocationData?.lng }
    }
    await createCity(newCity);
    navigate('/app/cities');
  }

  useEffect(() => {
    async function fetchCityData() {
      if (!lat && !lng) return;
      try {
        setIsLoadingGeocoding(true)
        const res = await fetch(`${BASE_URL}?latitude=${lat}&longitude=${lng}`);
        const data = await res.json();
        if (!data.countryCode) throw new Error("That doesn't seem to be a city. Click somewhere else 😊");
        setGeocodingError("");
        setCityName(data.city || data.locality || '');
        setCountry(data.countryName);
        setEmoji(convertToEmoji(data.countryCode));
        setCurrentMapLocationData({ lat: data.latitude, lng: data.longitude });
        console.log(data)
      } catch (error) {
        if (error instanceof Error)
          setGeocodingError(error.message);
      } finally {
        setIsLoadingGeocoding(false)
      }

    }
    fetchCityData();
  }, [lat, lng])

  if (geocodingError) return <Message message={geocodingError} />

  // if(!lat && !lng) return <Message message='Start by clicking somewhere on the map'/>

  if (isLoadingGeocoding) return <Spinner />

  return (
    <form className={`${styles.form} ${isLoading ? styles.loading : ''}`} onSubmit={handleSubmit}>
      <div className={styles.row}>
        <label htmlFor="cityName">City name</label>
        <input
          id="cityName"
          onChange={(e) => setCityName(e.target.value)}
          value={cityName}
        />
        <span className={styles.flag}>{emoji}</span>
      </div>

      <div className={styles.row}>
        <label htmlFor="date">When did you go to {cityName}?</label>
        <DatePicker
          onChange={(date) => setDate(date)}
          selected={date}
          dateFormat='dd/MM/yyyy'
          id="date"
        />
      </div>

      <div className={styles.row}>
        <label htmlFor="notes">Notes about your trip to {cityName}</label>
        <textarea
          id="notes"
          onChange={(e) => setNotes(e.target.value)}
          value={notes}
        />
      </div>

      <div className={styles.buttons}>
        <Button onClick={() => {}} type='primary'>Add</Button>
        <BackButton />
      </div>
    </form>
  );
}

export default Form;
