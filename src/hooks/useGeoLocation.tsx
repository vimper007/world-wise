import { useState } from "react"
import { PositionType } from "../types"

export const useGeoLocation = () => {
    const [position, setPosition] = useState<PositionType>();
    const [error, setError] = useState<string>();
    const [isLoading, setIsLoading] = useState<boolean>()

    function getPosition() {
        if (!navigator.geolocation) setError('Browser does not support geolocation')
        setIsLoading(true);
        navigator.geolocation.getCurrentPosition(
            (pos) => {
                setPosition({
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude,
                })
                setIsLoading(false);
            },
            error => {
                setError(error.message);
                setIsLoading(false);
            }
        )
    }

    return { isLoading, error, position, getPosition }
}
